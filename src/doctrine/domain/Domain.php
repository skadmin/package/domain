<?php

declare(strict_types=1);

namespace Skadmin\Domain\Doctrine\Domain;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Domain
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Website;
    use Entity\IsActive;
    use Entity\Created;

    public function create(string $website): void
    {
        $this->website = $website;
        $this->name    = $website;
    }
}
