<?php

declare(strict_types=1);

namespace Skadmin\Domain\Doctrine\Domain;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class DomainFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Domain::class;
    }

    public function get(?int $id = null): Domain
    {
        if ($id === null) {
            return new Domain();
        }

        $domain = parent::get($id);

        if ($domain === null) {
            return new Domain();
        }

        return $domain;
    }

    public function findByWebsiteOrCreate(string $website): Domain
    {
        $criteria = ['website' => $website];

        $domain = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $domain ?? $this->create($website);
    }

    public function create(string $website): Domain
    {
        $domain = $this->get();

        $domain->create($website);

        $this->em->persist($domain);
        $this->em->flush();

        return $domain;
    }
}
