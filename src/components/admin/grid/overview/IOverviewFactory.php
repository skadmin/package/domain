<?php

declare(strict_types=1);

namespace Skadmin\Domain\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
