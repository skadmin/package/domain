<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210315163142 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'domain.overview', 'hash' => '52915d9cd4fce30910a0a5d75812a107', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Domény', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.domain.title', 'hash' => '9a4e670242fa825f3737d8fcfd26854d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Domény', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.domain.description', 'hash' => 'c522cf730a1a79837041fe45aaec4390', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat domény', 'plural1' => '', 'plural2' => ''],
            ['original' => 'domain.overview.title', 'hash' => 'b3d0b18ba9a52c7272de5dbacb831a27', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Domény|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.domain.overview.name', 'hash' => '3e1acd0e22af4e915f688a7e84aca5db', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
