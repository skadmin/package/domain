<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\Domain\BaseControl;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116175940 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'domain',
            'name'         => 'Domain',
            'is_active'    => 1,
        ];

        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        // RESOURCE
        $resources = [
            [
                'name'        => 'domain',
                'title'       => 'role-resource.domain.title',
                'description' => 'role-resource.domain.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
